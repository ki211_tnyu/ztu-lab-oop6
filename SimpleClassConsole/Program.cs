﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassConsole
{
    class Date
    {
        public int Year { set; get; }
        public int Month { set; get; }
        public int Day { set; get; }
        public int Hours { set; get; }
        public int Minutes { set; get; }

        public Date() { }

        public Date(Date prevDate)
        {
            this.Year = prevDate.Year;
            this.Month = prevDate.Month;
            this.Day = prevDate.Day;
            this.Hours = prevDate.Hours;
            this.Minutes = prevDate.Minutes;
        }

        public Date(int year, int month, int day, int hours, int minutes)
        {
            Year = year;
            Month = month;
            Day = day;
            Hours = hours;
            Minutes = minutes;
        }
    }
    class Airplane
    {
        public string StartCity { set; get; }
        public string FinishCity { set; get; }
        public Date StartDate { set; get; }
        public Date FinishDate { set; get; }

        public Airplane() { }
        public Airplane(string startCity, string finishCity, Date startDate, Date finishDate)
        {
            StartCity = startCity;
            FinishCity = finishCity;
            StartDate = startDate;
            FinishDate = finishDate;
        }

        public int GetTotalTime()
        {
            int totalTime = 0;



            return totalTime;
        }

        public bool IsArrivingToday()
        {
            if (FinishDate.Day == StartDate.Day) return true;
            else return false;
        }
    }
    class Program
    {
        static Airplane[] ReadAirplaneArray()
        {
            Console.WriteLine("К-сть рейсів: ");
            int n = Convert.ToInt32(Console.ReadLine());
            Airplane[] airplane = new Airplane[n];

            string startCity, finishCity;
            int year_1, month_1, day_1, hours_1, minutes_1;
            int year_2, month_2, day_2, hours_2, minutes_2;

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Рейс № {i + 1}");
                Console.WriteLine("Початкове місто: ");
                startCity = Console.ReadLine();
                Console.WriteLine("Кінцеве місто: ");
                finishCity = Console.ReadLine();

                Console.WriteLine("Рік, місяць, день, година, хвилина початку");
                year_1 = Convert.ToInt32(Console.ReadLine());
                month_1 = Convert.ToInt32(Console.ReadLine());
                day_1 = Convert.ToInt32(Console.ReadLine());
                hours_1 = Convert.ToInt32(Console.ReadLine());
                minutes_1 = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Рік, місяць, день, година, хвилина кінця");
                year_2 = Convert.ToInt32(Console.ReadLine());
                month_2 = Convert.ToInt32(Console.ReadLine());
                day_2 = Convert.ToInt32(Console.ReadLine());
                hours_2 = Convert.ToInt32(Console.ReadLine());
                minutes_2 = Convert.ToInt32(Console.ReadLine());

                Date startDate = new Date(year_1, month_1, day_1, hours_1, minutes_1);
                Date finishDate = new Date(year_2, month_2, day_2, hours_2, minutes_2);
                airplane[i] = new Airplane(startCity, finishCity, startDate, finishDate);
            }

            return airplane;
        }
        static void PrintAirplane(Airplane airplane)
        {
            Console.WriteLine($"Початкове місто: {airplane.StartCity}");
            Console.WriteLine($"Кінцеве місто: {airplane.FinishCity}");
            Console.Write("Рік, місяць, день, година, хвилина початку: ");
            Console.Write($"{airplane.StartDate.Year}/{airplane.StartDate.Month}/{airplane.StartDate.Day} ");
            Console.Write($"{airplane.StartDate.Hours}:{airplane.StartDate.Minutes}\n");
            Console.Write("Рік, місяць, день, година, хвилина кінця: ");
            Console.Write($"{airplane.FinishDate.Year}/{airplane.FinishDate.Month}/{airplane.FinishDate.Day} ");
            Console.Write($"{airplane.FinishDate.Hours}:{airplane.FinishDate.Minutes}\n");
            Console.WriteLine($"Час польоту: {airplane.GetTotalTime()}хв");
            Console.WriteLine($"Прибули сьогодні: {airplane.IsArrivingToday()}");
        }
        static void PrintAirplanes(Airplane[] plane)
        {
            foreach (Airplane airplane in plane)
            {
                Console.WriteLine($"\nПочаткове місто: {airplane.StartCity}");
                Console.WriteLine($"Кінцеве місто: {airplane.FinishCity}");
                Console.Write("Рік, місяць, день, година , хвилина старту: ");
                Console.Write($"{airplane.StartDate.Year}/{airplane.StartDate.Month}/{airplane.StartDate.Day} ");
                Console.Write($"{airplane.StartDate.Hours}:{airplane.StartDate.Minutes}\n");
                Console.Write("Рік, місяць, день, година, хвилина кінця: ");
                Console.Write($"{airplane.FinishDate.Year}/{airplane.FinishDate.Month}/{airplane.FinishDate.Day} ");
                Console.Write($"{airplane.FinishDate.Hours}:{airplane.FinishDate.Minutes}\n");
                Console.WriteLine($"Час польоту: {airplane.GetTotalTime()}хв");
                Console.WriteLine($"Прибули сьогодні: {airplane.IsArrivingToday()}");
            }
        }

        static void GetAirplaneInfo(Airplane[] airplane, out int maxTime, out int minTime)
        {
            maxTime = 0;
            minTime = 9999;

            for (int i = 0; i < airplane.Length; i++)
            {
                if (airplane[i].GetTotalTime() > maxTime)
                {
                    maxTime = airplane[i].GetTotalTime();
                }
                if (airplane[i].GetTotalTime() < minTime)
                {
                    minTime = airplane[i].GetTotalTime();
                }
            }
        }

        static Airplane[] SortAirplanesByDate(Airplane[] obj)
        {
            for (int i = 1; i < obj.Length; i++)
            {
                for (int j = 0; j < obj.Length - i; j++)
                {
                    if (obj[j].StartDate.Year > obj[j].StartDate.Year ||
                        (obj[j].StartDate.Year == obj[j].StartDate.Year && obj[j].StartDate.Month > obj[j + 1].StartDate.Month) ||
                        (obj[j].StartDate.Year == obj[j].StartDate.Year && obj[j].StartDate.Month == obj[j + 1].StartDate.Month && obj[j].StartDate.Day > obj[j + 1].StartDate.Day) ||
                        (obj[j].StartDate.Year == obj[j].StartDate.Year && obj[j].StartDate.Month == obj[j + 1].StartDate.Month && obj[j].StartDate.Day == obj[j + 1].StartDate.Day && obj[j].StartDate.Hours > obj[j + 1].StartDate.Hours) ||
                        (obj[j].StartDate.Year == obj[j].StartDate.Year && obj[j].StartDate.Month == obj[j + 1].StartDate.Month && obj[j].StartDate.Day == obj[j + 1].StartDate.Day && obj[j].StartDate.Hours == obj[j + 1].StartDate.Hours && obj[j].StartDate.Minutes > obj[j + 1].StartDate.Minutes))
                    {
                        Airplane temp_obj;
                        temp_obj = obj[j];
                        obj[j] = obj[j + 1];
                        obj[j + 1] = temp_obj;
                    }
                }
            }
            return obj;
        }
        static Airplane[] SortAirplanesByTotalTime(Airplane[] obj)
        {
            for (int i = 1; i < obj.Length; i++)
            {
                for (int j = 0; j < obj.Length - i; j++)
                {
                    if (obj[j].GetTotalTime() > obj[j + 1].GetTotalTime())
                    {
                        Airplane temp_obj;
                        temp_obj = obj[j];
                        obj[j] = obj[j + 1];
                        obj[j + 1] = temp_obj;
                    }
                }
            }
            return obj;
        }
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.Title = "Лабораторна робота №6, Тишкевич Н.Ю, Варіант №6";
            Console.SetWindowSize(110, 25);
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Airplane[] airplane = ReadAirplaneArray();

            int n;

            bool stop = false;
            int MinReis, MaxReis;
            int s;

            while (!stop)
            {
                Console.WriteLine();
                Console.WriteLine("1. Вивести один запис.");
                Console.WriteLine("2. Вивести всі записи.");
                Console.WriteLine("3. Вивести мінімальний та максимальний час подорожі.");
                Console.WriteLine("4. Сортувати по даті.");
                Console.WriteLine("5. Сортувати по часу.");
                Console.WriteLine("6. Вихід.");

                n = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                switch (n)
                {
                    case 1:
                        Console.WriteLine($"Введіть номер польоту: 0-{airplane.Length - 1} : ");
                        s = Convert.ToInt32(Console.ReadLine());
                        PrintAirplane(airplane[s]);
                        break;
                    case 2:
                        PrintAirplanes(airplane);
                        break;
                    case 3:
                        int max_time, min_time;
                        GetAirplaneInfo(airplane, out max_time, out min_time);
                        Console.WriteLine("Максимальний час подорожi = " + max_time + ", Мiнiмальний час подорожi = " + min_time);
                        break;
                    case 4:
                        Console.WriteLine("====Сортування по датi початку польоту====");
                        PrintAirplanes(SortAirplanesByDate(airplane));
                        break;
                    case 5:
                        Console.WriteLine("====Сортування по тривалостi польоту====");
                        PrintAirplanes(SortAirplanesByTotalTime(airplane));
                        Console.ReadKey();
                        break;
                    case 6:
                        stop = true;
                        break;
                    default:
                        Console.WriteLine("NAN");
                        break;
                }
                Console.WriteLine();
            }

        }
    }
}




